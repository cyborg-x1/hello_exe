cmake_minimum_required(VERSION 3.0)
project(hello_exe LANGUAGES CXX)

add_executable(hello_exe)
target_sources(hello_exe PRIVATE "src/main.cpp")
target_link_libraries(hello_exe PRIVATE hello_lib)

install(TARGETS hello_exe
        RUNTIME DESTINATION /usr/bin
)
